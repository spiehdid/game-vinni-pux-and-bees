<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Винни-пух и пчёлы</title>
    <link rel="stylesheet" href="./css/index.css">
</head>

<body>
    <?php
    if (!empty($_POST['nickname'])) {
        session_start();
        $nickname = trim($_POST['nickname']);
        $mysql = new mysqli('localhost', 'root', '', 'my-api');
        $user = $mysql->query("SELECT * from `game_users` WHERE `nickname` = '$nickname'")->fetch_assoc();
        if (empty($user)) {
            $mysql->query("INSERT INTO `game_users` (`nickname`) VALUES('$nickname')");
        }
        setcookie("message", 'null');
        setcookie("score", 0);
        $_SESSION['nickname'] = $nickname;
        header('Location: home.php');
    }
    ?>

    <form class="form-login" method="POST">
        <input type="text" name="nickname" class="nickname form-item" id="nick">
        <input type="submit" value="Войти в игру" class="login-button form-item" id="login">
    </form>

    <script src="./js/jquery-3.4.1.min.js"></script>
    <script>
        $('#login').click(() => {
            localStorage.setItem('nick', $('#nick').val())
            localStorage.setItem('score', 0)
        })
    </script>

</body>

</html>