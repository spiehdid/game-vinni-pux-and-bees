<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/index.css">
    <title>Винни-пух и пчёлы</title>
</head>

<body>

    <div class="wrap">
        <?php
        if ($_COOKIE['message'] != 'null') {
            $message = $_COOKIE['message'];
            echo "            
                    <div class='message'>
                    $message
                    </div>
                    ";
        }
        setcookie("message", 'null');
        ?>
        <a href="home.php">
            <button class="login-button form-item exit">Вернуться в меню</button>
        </a>
        <a href="game.html">
            <button class="login-button form-item exit top-100">Начать игру</button>
        </a>
        <div class="title">
            Таблица рекордов(топ 5)
        </div>
        <div class="table">
            <div class="table-item">
                <div class="number">#</div>
                <div class="item-nickname">Nickname</div>
                <div class="item-score">Score</div>

            </div>
            <hr>
            <?php
            session_start();
            $mysql = new mysqli('localhost', 'root', '', 'my-api');
            $users = $mysql->query("SELECT * from `game_users` ORDER BY score DESC");
            if (!empty($users)) {
                $i = 1;
                $player_in_table = false;
                while ($user = mysqli_fetch_row($users)) {
                    if ($i != 5 && $user[1] == $_SESSION['nickname']) $player_in_table = true;
                    elseif ($i == 5 && !$player_in_table) {
                        $user[1] = $_SESSION['nickname'];
                        $user[2] = $_COOKIE['score'];
                    }
                    echo "            
                    <div class='table-item'>
                        <div class='number'>$i</div>
                        <div class='item-nickname'>$user[1]</div>
                        <div class='item-score'>$user[2]</div>
                    </div>";
                    $i++;
                    if ($i == 6) break;
                }
            }
            ?>
        </div>
    </div>
</body>

</html>