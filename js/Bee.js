class Bee {
    constructor(x, direction) {

        this.imageRight = beeSprite
        this.imageLeft = beeSpriteLeft

        this.x = x
        this.y = 0
        this.scale = 0.3
        this.width = 200 * this.scale
        this.height = 100 * this.scale

        this.speed = 2
        this.direction = direction    //if true right else left
        this.iterationDiraction = 0

        this.callisionWithPlayer = false
    }

    Update() {
        //Move
        getAccessMove() ? this.x -= 6 : ''

        this.iterationDiraction++
        this.x += this.direction ? this.speed : -this.speed
        if (this.iterationDiraction >= 500) {
            this.iterationDiraction = 0
            this.direction = !this.direction
        }
        //


        //Callision with player
        if (this.x <= player.x + player.image.width * scale &&
            this.x + this.width >= player.x &&
            player.y + player.image.height * scale - player.jumpHeight >= window.innerHeight - this.height) {
            this.callisionWithPlayer = true
        }
        else this.callisionWithPlayer = false
        //
    }

    Render() {
        // ctx.strokeRect(
        //     this.x,
        //     window.innerHeight - 80,
        //     this.width,
        //     this.height
        // )

        ctx.drawImage(
            this.direction ? this.imageRight : this.imageLeft,
            this.x,
            window.innerHeight - 80,
            this.width,
            this.height
        )
    }
}

let bees = []