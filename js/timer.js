class Timer {
    constructor() {
        this.minute = 0
        this.seconds = 0

        this.text = ''

        this.playerMinusHP = false
        this.quantityBeesWithcollision = 0

        this.timeSec = 0
    }

    ChangeTime() {
        player.hp--
        this.seconds++
        this.timeSec++
        this.playerMinusHP && (player.hp -= 20)
    }

    EndGame() {
        document.cookie = `score=${1000 - this.timeSec + player.point * 8}`;
        Stop()
        location.href = 'end_game.php'
    }


    Update() {
        this.quantityBeesWithcollision = 0

        if (player.hp <= 0) {
            document.cookie = `message=Вы проиграли`;
            this.EndGame()
        }
        if ((maps[0].distance + maps[1].distance) / 2.5 >= worldWidth) {
            document.cookie = `message=Вы выиграли`;
            this.EndGame()
        }

        if (this.seconds == 60) {
            this.minute++
            this.seconds = 0
        }


        bees.map(({ callisionWithPlayer }) => callisionWithPlayer && this.quantityBeesWithcollision++)

        if (this.quantityBeesWithcollision > 0) {
            if (!this.playerMinusHP) {
                player.hp -= 20
                this.playerMinusHP = true
            }
        }
        else this.playerMinusHP = false


        this.text = [
            { text: `Player ${localStorage.getItem('nick')}` },
            { text: `HP ${player.hp} ` },
            { text: `HONEY ${player.point} ` },
            { text: `TIME ${this.minute < 10 ? 0 : ''}${this.minute}:${this.seconds < 10 ? 0 : ''}${this.seconds} ` }
        ]

    }

    Render() {
        let top = 30
        this.text.map(({ text }) => {
            ctx.font = "30px sans-serif"
            ctx.fillText(
                text,
                10,
                top,
            )
            top += 30
        })

    }

}

let timer = new Timer()