const getAccessMove = () => !timerOn && window.innerWidth / 2 - player.image.width * scale < player.x

const Resize = () => {
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
}

const randomizer = (min, max) => Math.round(min - 0.5 + Math.random() * (max - min + 1))

const generateHoneysAndBees = () => {
    let quantity = randomizer(10, 20)
    let places = [600]
    for (let i = 0; i < worldIteration * 3; i++) { places.push(places[places.length - 1] + 300) }

    for (let i = 0; i < quantity / 2.5; i++) { //generate honyes
        let number = randomizer(0, places.length - 1)
        honyes.push(new Honey(places[number]))
        places = places.filter(el => el !== places[number])
    }

    for (let i = 0; i < quantity * 2; i++) { //generate bees
        let number = randomizer(0, places.length - 1)
        let direction = randomizer(0, 1)
        bees.push(new Bee(places[number], direction))
        places = places.filter(el => el !== places[number])
    }
}


image.onload = () => setTimeout(() => {
    Resize()
    player.y = window.innerHeight - image.height * scale
    Start()
}, 100)

window.addEventListener("resize", Resize)