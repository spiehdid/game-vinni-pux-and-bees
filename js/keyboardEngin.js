const keys = {
    'W': 87,
    'S': 83,
    'A': 65,
    'D': 68,
    'Esc': 27,
    'Enter': 13
}

let keyDown = {}

const isKeyDown = keyName => keyDown[keys[keyName]]

window.addEventListener("keydown", ({ keyCode }) => keyDown[keyCode] = true)
window.addEventListener("keyup", ({ keyCode }) => keyDown[keyCode] = false)
