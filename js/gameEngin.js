const Start = () => {
    generateHoneysAndBees()
    gameFlow = setInterval(() => game(), 1000 / 60)
    timerFlow = setInterval(() => timer.ChangeTime(), 1000)
}

const Stop = () => {
    clearInterval(gameFlow)
}


const game = () => {
    process()
    update()
    render()
}

const process = () => {

    isKeyDown('W') && player.Jump()
    isKeyDown('A') && player.Move(-speed)
    isKeyDown('D') && player.Move(speed)
    isKeyDown('Esc') && timer.PauseOrConfirm()

}

const update = () => {

    maps[0].Update(maps[1])
    maps[1].Update(maps[0])

    honyes.map(honye => honye.Update())

    bees.map(bee => bee.Update())

    player.Update()

    timer.Update()

}

const render = () => {

    ctx.clearRect(0, 0, canvas.width, canvas.height)  //clear canvas

    maps.map(map => map.Render())

    honyes.map(honye => honye.Render())

    bees.map(bee => bee.Render())

    player.Render()

    timer.Render()

}