let
    canvas = document.getElementById("canvas"),
    ctx = canvas.getContext("2d"),

    speed = 6,
    scale = 0.1, //Масштаб 
    worldWidth = 3000,
    worldIteration = Math.round(worldWidth / 300),
    gameFlow, 
    timerFlow, 
    timerOn = false,

    island = new Image(),
    honey = new Image(),
    beeSprite = new Image(),
    beeSpriteLeft = new Image(),
    image = new Image(),
    lefVinni = new Image()

island.src = "./images/land-vector-1.png"
honey.src = "./images/caterpillar_PNG19.png"
beeSprite.src = "./images/right_pchela.png"
beeSpriteLeft.src = "./images/left_pchela.png"
image.src = "./images/1416160054_452602345.png"
lefVinni.src = "./images/left-vinni.png"
