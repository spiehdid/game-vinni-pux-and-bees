class Map {
    constructor(image, x) {
        this.x = x
        this.y = 0

        this.image = image

        this.distance = 0
    }

    Update(map) {
        getAccessMove() ? this.x -= speed : ''

        if (this.x + window.innerWidth < 0) {
            this.x = map.x + window.innerWidth - speed
            this.distance += window.innerWidth
        }
    }

    Render() {
        ctx.drawImage(
            this.image,
            this.x,
            this.y,
            canvas.width,
            canvas.height
        )
    }
}

let mapImage = new Image()

mapImage.src = "./images/background-12.png"

let maps = [
    new Map(mapImage, 0),
    new Map(mapImage, window.innerWidth)
]