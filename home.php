<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Винни-пух и пчёлы</title>
    <link rel="stylesheet" href="./css/index.css">
    <script src="./js/jquery-3.4.1.min.js"></script>
</head>

<body>
    <div class="form-login">
        <button class="login-button form-item" id="start"> Начать игру</button>
        <a href="scores.php">
            <button class="login-button form-item">Таблица рекордов</button>
        </a>
        <video src="./videos/video.mp4" class="video"></video>

    </div>
    <script>
        addEventListener('keydown', ({
            code
        }) => code === 'Space' && (localStorage.setItem('video', true), location.href = 'game.html'))

        var v = document.getElementsByTagName("video")[0];
        v.addEventListener('ended', () => {
            localStorage.setItem('video', true)
            location.href = 'game.html'
        })
        $("#start").click(function() {
            if ((JSON.parse(localStorage.getItem('video')) || false) !== true) {
                $('video').addClass('show');
                v.play()
            } else {
                location.href = 'game.html'
            }
        });
    </script>
</body>

</html>